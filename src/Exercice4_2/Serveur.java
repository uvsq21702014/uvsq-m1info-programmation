package Exercice4_2;

import java.util.ArrayList;

public class Serveur {

    private ArrayList<Client> clients;

    public Serveur(){
        this.clients = new ArrayList<>();
    }

    public boolean connecter(Client client){
        //cojer el cliente y anadirlo a la lista de los clientes
        this.clients.add(client);
        return true;
    }

    public void diffuser(String message){
        //recorrer todos los clientes y por c/u llamar el metodo recibir
        for (int i=0; i<clients.size(); i++){
            Client actual = clients.get(i);
            System.out.println(actual.recevoir(message));
        }
    }
}
