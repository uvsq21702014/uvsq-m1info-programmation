package Exercice4_2;

public class Client {

    public String nom;
    private Serveur serveur;

    public Client(String nom){
        this.nom= nom;
    }

    public boolean seConnecter (Serveur serveur){
        this.serveur= serveur;
        return true;
    }

    public void envoyer(String message){
        this.serveur.diffuser(message);
    }

    public String recevoir(String message){
        return "moi: " + this.nom + " reçu: " + message;
    }
}
