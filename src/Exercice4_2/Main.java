package Exercice4_2;

public class Main {

    public static void main (String [] arg){

        Serveur S = new Serveur();

        Client C1 = new Client( "C1");
        Client C2 = new Client( "C2");
        Client C3 = new Client( "C3");

        C1.seConnecter(S);
        C2.seConnecter(S);
        C3.seConnecter(S);

        C3.envoyer("Message");

    }
}
