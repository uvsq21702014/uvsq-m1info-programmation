package Excercice5_2;

//Fractions A/B et C/D
//x -> valor de retorno pour A et B
//

public class Fraction {

    private int A;
    private int B;
    public static final Fraction ZERO = new Fraction(  0, 1);

    public static final Fraction UN = new Fraction(  1, 1);


    public Fraction (int A, int B){
        this.A = A;
        this.B = B;
    }

    public Fraction(int A){
        this.A = A;
        this.B = 1;
    }
    public Fraction(){
        this.A = 0;
        this.B = 1;
    }

    public int getA(){
        int x=this.A;
        return x;
    }
    public int getB(){
        int x=this.B;
        return x;
    }

    public double doubleValue(){
        double division = (double)A/(double)B;
        return division;
    }

    public int gcd (int C, int D){

        int gcd=0;
        int m=C;
        int n=D;

    /*    if(C<D)
            C=D;
        else
            D=C;
    */
        while(n!=0){
            gcd=n%m;
            n=m;
            m=gcd;
        }
        return gcd;
    }

    public Fraction sommeFractions (Fraction F){

        int nA =A*F.B + B*F.A;
        int nB = B*F.B;

        int gcd = gcd(nA,nB);
        return new Fraction(nA/gcd, nB/gcd);
    }

    public boolean egaliteFractions (Object object){
        Fraction fraction = (Fraction) object;
        int pgcdFraction = gcd(fraction.A, fraction.B);
        int pgcdThis = gcd(this.A, this.B);
        if(fraction.A/pgcdFraction == this.A/pgcdThis && fraction.B/pgcdFraction == this.B/pgcdThis){
            return true;
        }
        else{
            return false;
        }
    }

    public String convString(){
        if(this.B != 0)
            return this.A + "/" + this.B;
        return this.A + " ";
    }

    public int comparaisonFractions(Fraction fraction){
        if(fraction.doubleValue()<this.doubleValue())
            return 1;
        else if(fraction.doubleValue()>this.doubleValue())
            return -1;
        return 0;
    }


}
