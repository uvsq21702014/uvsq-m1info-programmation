package Exercice3_1;


public class CompteBancaire {

    private float solde;


    public CompteBancaire(float solde) {
        if(solde<=0) {
            throw new IllegalArgumentException("Le solde doit être major de zéro");
            //System.out.println("Le solde doit être major de zéro");
        } else {
            this.solde = solde;
        }
    }


    public float consultation(){
        return solde;
    }

    public void credit(float quantiteIntroduire){
        if(quantiteIntroduire>=0){
            solde=solde+quantiteIntroduire;
        }
    }


    public void debit(float quantiteDebit) {
        if (solde >= quantiteDebit && quantiteDebit >= 0) {
            solde = solde - quantiteDebit;
        }
    }

    public void virement(float quantiteTransf, CompteBancaire compte) {
        if (solde>=0 && solde>quantiteTransf && quantiteTransf>=0) {
            solde=solde - quantiteTransf;
            compte.credit(quantiteTransf);
        }
    }
}
