package Exercice3_1;

import org.junit.*;

import static org.junit.Assert.*;

public class CompteBancaireTest {

    @ Test
    public void testDebit(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);
        compteBancaire.debit(-1f);
        assertTrue(compteBancaire.consultation()==10f);
        compteBancaire.debit(11f);
        assertTrue(compteBancaire.consultation()==10f);
        compteBancaire.debit(3f);
        assertTrue(compteBancaire.consultation()==7f);

    }

    @ Test
    public void testVirement(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);
        CompteBancaire a= new CompteBancaire(1f);

        compteBancaire.virement(-1f, a);
        assertTrue(compteBancaire.consultation()==10f);
        assertTrue(a.consultation()==1f);

        compteBancaire.virement(11f,a);
        assertTrue(compteBancaire.consultation()==10f);
        assertTrue(a.consultation()==1f);

        compteBancaire.virement(8f,a);
        assertTrue(compteBancaire.consultation()==2f);
        assertTrue(a.consultation()==9f);

    }

    @ Test
    public void testCredit(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);

        compteBancaire.credit(-1f);
        assertTrue(compteBancaire.consultation()==10f);

        compteBancaire.credit(0f);
        assertTrue(compteBancaire.consultation()==10f);

        compteBancaire.credit(2f);
        assertTrue(compteBancaire.consultation()==12f);


    }

    @ Test
    public void testSuccesCompteBancaire(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);

        assertNotNull(compteBancaire);

    }

    @ Test (expected=IllegalArgumentException.class)
    public void testFailureCompteBancaire(){
        CompteBancaire compteBancaire= new CompteBancaire(-10f);
    }
}
