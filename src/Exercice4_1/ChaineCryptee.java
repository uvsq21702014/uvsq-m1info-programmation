package Exercice4_1;

/*Par rapport à le comportement de la classe dans le cas que la
chaîne est null, elle sera traitée comment une chaîne vide
*/

public class ChaineCryptee {

    private String crypte;
    private int decalage;

    private ChaineCryptee(String newEnClair, int newDecalage) {

        decalage = newDecalage;
        if (newEnClair != null) {
            crypte = encriptar(newEnClair, newDecalage);
        }
        else {
            crypte = "";
        }
    }

    public String crypte(){

        return crypte;
    }
/*metodo para encriptar*/
    private static String encriptar(String newEnClair, int newDecalage){
        String crypte= new String();
        int i=0;
        char[] enClaireArray = newEnClair.toCharArray();
        while(i<enClaireArray.length){
            crypte= crypte+caleCaractere(enClaireArray[i], newDecalage);
            i++;
        }
        return crypte;
    }
    /*met para desencriptar*/
    private static String desencriptar(String crypte, int newDecalage){
        String desencriptado= new String();
        int i=0;
        char[] crypteArray = crypte.toCharArray();
        while(i<crypteArray.length){
            desencriptado= desencriptado+caleCaractere(crypteArray[i], newDecalage);
            i++;
        }
        return desencriptado;
    }

    private static char decaleCaractere(char c, int decalage){
        return (c<'A'||c>'Z')? c:(char)(((c-'A'+decalage)%26)+'A');
    }

    private static char caleCaractere(char c, int decalage){
        return (c<'A'||c>'Z')? c:(char)(((c-'A'-decalage)%26)+'A');
    }

    public ChaineCryptee deCryptee(String p, int newDecalage){
        String a = desencriptar(p, newDecalage);
        return new ChaineCryptee (a, newDecalage);
    }

    public ChaineCryptee deEnclair(String newEnClair, int newDecalage){
        return new ChaineCryptee(newEnClair,newDecalage);
    }
}
