package Exercice3_5_1;

import java.util.Iterator;
import java.util.Scanner;

public class SaisieRPN {

    public SaisieRPN(){

    }

    public void gererSaisie(){
        Scanner sc = new Scanner(System.in);
        MoteurRPN moteurRPN = new MoteurRPN();
        while (sc.hasNext()){
            String next = sc.next();
            if(next.equals("Fin"))
                break;
            else {
                for (Operation operation: Operation.values()) {
                    if(next.equals(operation.getSimbol())){
                        try{
                            moteurRPN.apliquerOp(operation);
                        } catch (Exception exception){
                            System.out.println(exception.getMessage());
                        }}
                }
                try{
                    double number = Double.parseDouble(next);
                    moteurRPN.enregistrer(number);
                } catch (Exception e)
                {
                    System.out.println("Il faut saisir que des nombres");
                }
            }

            }
        }
    }