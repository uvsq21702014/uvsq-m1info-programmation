package Exercice3_5_1;

public enum Operation {

    PLUS("+"){
        @Override
        public double eval(double A, double B) {

            return A + B;
        }
    },
    MOINS("-"){
        @Override
        public double eval(double A, double B) {

            return A - B;
        }
    },
    MULT("*"){
        @Override
        public double eval(double A, double B) {

            return A * B;
        }
    },
    DIV("/"){
        @Override
        public double eval(double A, double B) {

            return A / B;
        }
    };

    //attribut symbole
    private String simbol;
    Operation(String simbol){
        this.simbol = simbol;
    }
    public String getSimbol() {
        return simbol;
    }
    public abstract double eval (double A, double B);
}
