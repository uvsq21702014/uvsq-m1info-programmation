package Exercice3_5_1;


import org.junit.Test;
import static org.junit.Assert.*;

public class OperationTest
    {
    @Test
    public void sommeEvalTest(){
        assertEquals(Operation.MOINS.eval(2.0,3.0), -1.0, 0.000000001);
    }
    @Test
    public void sommeNameTest(){
        assertEquals(Operation.MOINS.name(),"MOINS");
    }
    @Test
    public void sommeSimbolTest(){
        assertEquals(Operation.MOINS.getSimbol(),"-");
    }
    }