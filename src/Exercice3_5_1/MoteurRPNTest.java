package Exercice3_5_1;

import org.junit.Test;
import java.util.Iterator;
import static org.junit.Assert.*;

public class MoteurRPNTest {
    @Test
    public void enregistrerTest(){
        MoteurRPN moteurRPN = new MoteurRPN();
        moteurRPN.enregistrer(3.0);
        Iterator<Double> pileIterator = moteurRPN.ensembleOpStockees();
        assertEquals(pileIterator.next(),3.0,0.000000001);
    }
    @Test
    public void apliquerOpTest() throws Exception{
        MoteurRPN moteurRPN = new MoteurRPN();
        moteurRPN.enregistrer(3.0);
        moteurRPN.enregistrer(5.4);
        moteurRPN.apliquerOp(Operation.PLUS);
        assertEquals(moteurRPN.ensembleOpStockees().next(),8.4,0.00000001);
    }
}