package Exercice3_5_1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class MoteurRPN{

    private Deque<Double> pile;
    public MoteurRPN(){
        pile = new ArrayDeque<>();
    }
    public void enregistrer(double operand){
        pile.addFirst(operand);
    }
    public void apliquerOp(Operation operation) throws Exception{
        if(pile.size()>1){

            double B = pile.pollFirst();
            if(B==0.0 && operation.name().equals("DIV")){
                pile.addFirst(B);
                throw new Exception("Division par zero impossible"); }

            double A = pile.pollFirst();
            double result = operation.eval(A,B);
            pile.addFirst(result);
        }
        else throw new Exception("Il faut au moins deux operands pour faire le calcul");
    }
    public Iterator<Double> ensembleOpStockees(){
        return pile.iterator();
    }
}
